# Slack bot for ASTRON/JIVE daily image

Post the ASTRON/JIVE daily image from http://www.astron.nl/dailyimage/ to
the #daily-image channel on the Astron Slack at radio-observatory.slack.com

To use, put a file called `slack_secret` in the same directory as the main
script, and put one line with a slack secret in this file. The slack secret
should look like

```
T3XTAT1R3/K010AZ6BD2I/alqRFnPDndremLfc92aLd4sx
```
