#!/usr/bin/env python

"""Grab daily image from ASTRON website and post it to Slack"""

import re
import sys
import json
import os
import pathlib
import logging
from argparse import ArgumentParser
import requests
from bs4 import BeautifulSoup


def html2markdown(msg: BeautifulSoup) -> BeautifulSoup:
    """Markdownify links using BeautifulSoup"""
    for link in msg.findAll("a"):
        if 'href' in link.attrs:
            link.replaceWith(f"<{link.attrs['href']}|{link.text.replace('http://','').replace('https://','')}>")
        else:
            link.replaceWith(link.text)

    # Markdownify bold words
    for boldword in msg.findAll("b"):
        boldword.replaceWith(f"**{boldword.text}**")

    # Handle <pre>
    for code in msg.findAll("pre"):
        code.replaceWith(f"\n```\n{code.text}\n```\n")

    # Markdownify italicized words
    for italicword in msg.findAll("i"):
        italicword.replaceWith(f"*{italicword.text}*")

    # Do something with superscripts
    for superscript in msg.findAll("sup"):
        superscript_map = {"0": "⁰", "1": "¹", "2": "²", "3": "³", "4": "⁴", "5": "⁵", "6": "⁶", "7": "⁷", "8": "⁸", "9": "⁹"}
        if superscript.text.isnumeric():
            superscript_new = ''.join([superscript_map[char] for char in superscript.text])
        else:
            superscript_new = '^(' + superscript.text
        superscript.replaceWith(superscript_new)

    # Remove breaks
    for hardbreak in msg.findAll("br"):
        hardbreak.replaceWith("")

    return msg


def main():
    """Grab daily image from ASTRON website and post it to Slack"""

    parser = ArgumentParser(description="Post ASTRON daily image to Slack")
    parser.add_argument("-f", "--force", help="Actually push to Slack", action="store_true")
    parser.add_argument("-a", "--always", help="Also post old images", action="store_true")
    args = parser.parse_args()

    logger = logging.getLogger(__name__)
    logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))

    url = 'https://www.astron.nl/dailyimage/'
    response = requests.get(url)
    assert response.ok

    soup = BeautifulSoup(response.text, "html.parser")

    if not args.always and soup.findAll(text=re.compile("No image found for date: ")):
        logger.error("No image found today")
        sys.exit(0)

    title = soup.findAll("h2")[0].getText()

    images = soup.findAll("img")
    image_url = "https://www.astron.nl/dailyimage/" + images[0]['src'].replace('@', '%40').replace('&', '%26')

    datatable = soup.findAll('table')[1].findAll("td")

    submitter = datatable[1].getText().strip()

    fulltext = "*" + title + "*\n\n" + submitter + "\n\n"
    fulltext += html2markdown(datatable[3]).text
    maintexts = fulltext.replace('\r', '').strip().split('\n\n')

    if len(images) > 1: # has image:
        image_block = [{
                "type": "image",
                "image_url": image_url,
                "alt_text": title,
                "title": {
                    "type": "plain_text",
                    "text": title
                }
            }]
    else:
        image_block = []

    full_msg = {
        "blocks": image_block + [
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": text
                }
            }
            for text in maintexts if len(text.strip()) > 0]
    }

    if 'SLACK_SECRET' in os.environ:
        logging.debug("Using slack secret from environment")
        slack_secret = os.environ['SLACK_SECRET']
    else:
        logging.debug("Using slack secret from file")
        with open(os.path.join(pathlib.Path(__file__).parent.absolute(), 'slack_secret.txt')) as slack_file:
            slack_secret = slack_file.readlines()[0].strip()

    slack_url = "https://hooks.slack.com/services/" + slack_secret

    print(json.dumps(full_msg))
    if args.force:
        slack_response = requests.post(slack_url, json=full_msg)

        if not slack_response.ok:
            print("Slack responded with an error, try the json below on https://app.slack.com/block-kit-builder")
            print(json.dumps(full_msg))
            raise(IOError())


if __name__ == "__main__":
    main()
